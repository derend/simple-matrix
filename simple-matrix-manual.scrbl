#lang scribble/manual
@(require scribble/example
          racket/sandbox
          (for-label racket
                     "main.rkt"))
@(define (author-email) "deren.dohoda@gmail.com")

@title[#:tag "top"]{Simple Matrix Arithmetic}
@author{@(author+email "Deren Dohoda" (author-email))}

@(define this-eval (parameterize ((sandbox-output 'string)
                                  (sandbox-error-output 'string)
                                  (sandbox-memory-limit 50))
                     (make-evaluator 'racket/base #:requires '("main.rkt"))))

@defmodule[simple-matrix]

@section{Introduction}
This library is used to perform some simple matrix arithmetic like multiplication, transposition, addition, and inversion.
It is truly "simple" and very naive in its implementation but it will do its job without much overhead from typed racket.

Matrices are represented as lists of rows, which are lists of numbers.
So a 3x3 identity matrix would look like @racket['((1 0 0) (0 1 0) (0 0 1))].
@defproc[(matrix? [maybe-m any/c]) boolean?]{Determines if the argument is a matrix in the sense this library understands.}
@defproc[(column [vector (listof number?)]) matrix?]{A convenience for turning a simple list of numbers into a column vector.}
@examples[#:eval this-eval
          (column '(1 2 -2))]
@section{Arithmetic}
@defproc[(matrix+ [m matrix?] ...) matrix?]{Adds two matricies element-wise.}
@defproc[(matrix* [m matrix?] ...) matrix?]{Multiplies two matrices using matrix multiplication. It is an error if the sizes
                                            do not correspond to proper matrix multiplication.}
@defproc[(matrix-expt [m matrix?] [e nonnegative-integer?]) matrix?]{Repeated multiplication of a matrix.}
@defproc[(transpose [m matrix?]) matrix?]{Exchange rows and columns of a matrix.}
@defproc[(determinant [m matrix?]) number?]{Calculate the determinant of a matrix.}
@section{Utilities}
@defproc[(adjugate (m matrix?)) matrix?]{The transpose of the cofactor matrix. Not efficiently calculated.}
@defproc[(invert-matrix [m matrix?]) (or/c matrix? boolean?)]{Attempts to invert a matrix. If the matrix has no inverse, then
                                                              this procedure evaluates to @racket[#f].}